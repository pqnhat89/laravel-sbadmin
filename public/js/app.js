$(document).on('click', '[name=status]', function () {
    var element = $(this).closest('tr');

    var id = element.data('id');

    var reactions = element.find('[name=reaction]:checked');
    var reactionsStr = '';
    reactions.each(function (k, v) {
        reactionsStr += v.value + ',';
    });
    reactionsStr = reactionsStr.slice(0, -1);

    var target = element.find('[name=target]').val();

    var timeout = element.find('[name=timeout]').val();

    var status = element.data('status');

    var url = '/account-manager/';

    $.ajax({
        type: 'PUT',
        url: url + id,
        data: {
            '_token': window.Laravel.csrf_token,
            'id': id,
            'reaction': reactionsStr,
            'target': target,
            'timeout': timeout,
            'status': status
        },
        context: this,
        success: function (status) {
            if (status == 1) {
                $(this).removeClass('btn-danger').addClass('btn-success').html('Đang chạy');
                element.data('status', 1);
            } else {
                $(this).removeClass('btn-success').addClass('btn-danger').html('Đã dừng');
                element.data('status', 0);
            }
        }
    });
});
@extends('layouts.app')

@section('content')
    <div class="row" id="auto-like">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <strong>Quản lý tài khỏan Facebook</strong>
                    <hr>
                    <p>
                        Bạn có thể chuyển quyền sử dụng dịch vụ (thời gian sử dụng còn lại) của tài khoản đã thanh toán
                        cho bất kỳ tài khoản nào khác bằng cách chỉnh sửa tài khoản.
                    </p>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('account-manager.store') }}">
                        {{ csrf_field() }}
                        <strong>Cách 1: Điền thông tin đăng nhập của bạn</strong>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-12" for="fb_username">Tài khoản dùng để đăng nhập facebook</label>
                            <div class="col-md-12">
                                <input type="text" id="fb_username" name="fb_username"
                                       class="form-control form-control-line"
                                       placeholder="Email, facebook ID hoặc số điện thoại">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="fb_password">Mật khẩu mật khẩu đăng nhập tài khoản này</label>
                            <div class="col-md-12">
                                <input type="password" id="fb_password" name="fb_password"
                                       class="form-control form-control-line"
                                       placeholder="Mật khẩu">
                            </div>
                        </div>
                        <br><br>
                        <strong>Cách 2: Nhập access token</strong>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-12" for="fb_access_token">Access Token của bạn</label>
                            <div class="col-md-12">
                                <textarea type="text" id="fb_access_token" name="fb_access_token"
                                          class="form-control form-control-line"
                                          placeholder="Access Token của bạn"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="form-control form-control-line btn btn-primary">Thêm nick
                                    Facebook
                                </button>
                                <a class="form-control form-control-line btn btn-success" href="{{ route('account-manager.index') }}">Trở về trang quản lý tài khoản
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer small text-muted">
                    Updated yesterday at 11:59 PM
                </div>
            </div>
        </div>
    </div>
@endsection

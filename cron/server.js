var http = require('http');
var curl = require('curl');
var qs = require('querystring');
var url = require('url');
var fs = require('fs');

var dataAccount = {};
var api = [];
api[1] = 'https://graph.facebook.com/{id}/reactions?type=LIKE&access_token={access_token}&method=post';
api[2] = 'https://graph.facebook.com/{id}/reactions?type=LOVE&access_token={access_token}&method=post';
api[3] = 'https://graph.facebook.com/{id}/reactions?type=WOW&access_token={access_token}&method=post';
api[4] = 'https://graph.facebook.com/{id}/reactions?type=HAHA&access_token={access_token}&method=post';
api[5] = 'https://graph.facebook.com/{id}/reactions?type=ANGRY&access_token={access_token}&method=post';
api[6] = 'https://graph.facebook.com/{id}/reactions?type=SAD&access_token={access_token}&method=post';

// gọi api và xử lý
getData();

function getData() {
    curl.get('http://fb.app/getListAutoLike', function (err, res, body) {
        dataAccount = JSON.parse(body);
        var i = 0;
        var intv = setInterval(function () {
            if (dataAccount.length - 1 <= i) {
                clearInterval(intv);
            }
            reactions(dataAccount[i]);
            i++;
        }, 10);
    });
}

function reactions(query) {
    var reaction = query.reaction ? query.reaction.split(',') : [1];
    var type = reaction[Math.floor(Math.random() * reaction.length)];
    if (query.accountid && query.token) {
        process(type, query.token);
    }
}

function process(type, token) {
    curl.get('https://graph.facebook.com/me/home?fields=id&access_token=' + token, function (err, res, body) {
        var data = JSON.parse(body);
        if (data.data) {
            var postId = data.data[0].id;
            var apiUrl = parseUrl(parseUrl(api[type], '{id}', postId), '{access_token}', token);
            curl.get(apiUrl, function (err, res, body) {
                console.log(body);
            });
        } else {
            console.log('token lỗi');
        }
    });
}

function parseUrl(url, searchvalue, newvalue) {
    return url.replace(searchvalue, newvalue);
}
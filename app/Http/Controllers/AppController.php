<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function getListAutoLike()
    {
        $accounts = Account::with([
            'logs' => function ($r) {
                $r->orderBy('id', 'desc');
                $r->first();
            }
        ])->where('status', 1)
            ->select(
                'userid',
                'accountid',
                'token',
                'reaction',
                'target',
                'timeout'
            )
            ->get();

        return response()->json($accounts);
    }
}
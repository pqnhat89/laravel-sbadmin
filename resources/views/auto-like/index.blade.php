@extends('layouts.app')

@section('content')
    <div class="row" id="auto-like">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <strong>CẤU HÌNH TỰ ĐỘNG THẢ TIM</strong>
                    <hr>
                    <p>
                        Tự động thả tim là sử dụng nick của bạn, thả tim, like hoặc tương tác vào các bài viết, comment,
                        của bạn bè hoặc các bài viết trên wall của bạn. Mục tiêu tăng độ tương tác của nick, giúp bài
                        viết được nhiều người bết đến hơn.<br>Khi bạn thực hiện tính năng này, đồng nghĩa với việc bạn
                        đồng ý cho phép sử dụng nick của bạn: like, thả tim, haha, wow vào nick của các thành viên khác.
                    </p>
                    <a id="account-manager" class="btn btn-sm btn-primary" href="{{ route('account-manager.create') }}">THÊM
                        NICK +</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Facebook</th>
                                <th>Chọn loại tim</th>
                                <th>Mục tiêu</th>
                                <th>Thời gian chờ (giây)</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Facebook</th>
                                <th>Chọn loại tim</th>
                                <th>Mục tiêu</th>
                                <th>Thời gian chờ (giây)</th>
                                <th>Trạng thái</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($accounts as $k=>$account)
                                <tr data-id="{{ $account->accountid }}" data-status="{{ $account->status }}">
                                    <td>{{ $k + 1 }}</td>
                                    <td>
                                        <div class="card-body small bg-faded p-0">
                                            <div class="media">
                                                <img class="d-flex mr-3" src="{{ $account->picture }}">
                                                <div class="media-body">
                                                    <span class="text-nowrap"><a target="_blank"
                                                                                 href="https://fb.com/{{ $account->accountid }}">{{ $account->name }}</a>
                                                    / {{ $account->accountid }}</span>
                                                    <br>
                                                    <span class="text-nowrap">Email: {{ $account->email }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row ml-0">
                                            @foreach($reactions as $k=>$reaction)
                                                <div class="col-2 pl-3 pr-3 label">
                                                    <label for="reaction-{{ $account->accountid }}-{{ $k+1 }}" class="m-0">
                                                        <img src="{{ asset('images/'.$reaction.'.png') }}">
                                                    </label>
                                                    <br>
                                                    <input id="reaction-{{ $account->accountid }}-{{ $k+1 }}"
                                                           type="checkbox" name="reaction"
                                                           {{ in_array($k+1, explode(',', $account->reaction)) ? 'checked' : null }}
                                                           value="{{ $k+1 }}">
                                                </div>
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>
                                        <select name="target" class="form-control form-control-sm">
                                            @foreach($targets as $k=>$target)
                                                <option value="{{ $k+1 }}" {{ $account->target == $k+1 ? 'selected' : null }}>{{ $target }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="timeout" class="form-control form-control-sm">
                                            @foreach($timeouts as $timeout)
                                                <option value="{{ $timeout }}" {{ $account->timeout == $timeout ? 'selected' : null }}>{{ $timeout }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <button name="status"
                                                class="btn btn-sm{{ $account->status ? ' btn-success' : ' btn-danger' }}">
                                            {{ $account->status ? 'Đang chạy' : 'Đã dừng' }}
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">
                    Updated yesterday at 11:59 PM
                </div>
            </div>
        </div>
    </div>
@endsection

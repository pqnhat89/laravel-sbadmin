<?php
/**
 * Created by PhpStorm.
 * User: nhat
 * Date: 27/10/2017
 * Time: 14:24
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $table = 'accounts';

    protected $guarded = [];

    public function logs()
    {
        return $this->hasMany('\App\Models\Log', 'userid', 'userid');
    }
}
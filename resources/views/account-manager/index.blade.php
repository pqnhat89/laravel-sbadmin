@extends('layouts.app')

@section('content')
    <div class="row" id="auto-like">
        <div class="col-md-12 col-md-offset-2">
            <div class="card">
                <div class="card-header">
                    <strong>Quản lý tài khỏan Facebook</strong>
                    <hr>
                    <p>
                        Bạn có thể chuyển quyền sử dụng dịch vụ (thời gian sử dụng còn lại) của tài khoản đã thanh toán
                        cho bất kỳ tài khoản nào khác bằng cách chỉnh sửa tài khoản.
                    </p>
                    <a id="account-manager" class="btn btn-sm btn-primary" href="{{ route('account-manager.create') }}">THÊM NICK +</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Facebook ID</th>
                                <th>Facebook Name</th>
                                <th>Email</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Facebook ID</th>
                                <th>Facebook Name</th>
                                <th>Email</th>
                                <th>Xóa</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($accounts as $k=>$account)
                                <tr>
                                    <td>{{ $k + 1 }}</td>
                                    <td>{{ $account->accountid }}</td>
                                    <td>{{ $account->name }}</td>
                                    <td>{{ $account->email }}</td>
                                    <td>
                                        <form method="POST"
                                              action="{{ route('account-manager.destroy', [$account->accountid]) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-sm btn-danger">Xóa</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">
                    Updated yesterday at 11:59 PM
                </div>
            </div>
        </div>
    </div>
@endsection

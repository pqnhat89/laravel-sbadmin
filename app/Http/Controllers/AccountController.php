<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use JC\HttpClient\JCRequest;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::where('userid', Auth::user()->id)->get();
        return view('account-manager.index', ['accounts' => $accounts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account-manager.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->sign_creator($request->fb_username, $request->fb_password);

        if (isset($data->access_token) || $request->fb_access_token) {
            $accountData = $this->getAccountData($request->fb_access_token ?? $data->access_token);
            if (isset($accountData->id)) {
                Account::withTrashed()
                    ->updateOrCreate(
                        [
                            'userid' => Auth::user()->id,
                            'accountid' => $accountData->id
                        ],
                        [
                            'username' => $request->fb_access_token ? null : $request->fb_username ?? null,
                            'password' => $request->fb_access_token ? null : $request->fb_password ?? null,
                            'name' => $accountData->name ?? null,
                            'email' => $accountData->email ?? null,
                            'picture' => $accountData->picture->data->url ?? $accountData->picture ?? null,
                            'token' => $data->access_token ?? $request->fb_access_token ?? null,
                            'cookie' => json_encode($data->session_cookies ?? null),
                            'deleted_at' => null
                        ]
                    );
                return redirect(route('account-manager.index'))->with('success', 'Thêm tài khoản Facebook ID ' . $accountData->id . ' thành công.');
            }
        }

        return Redirect::back()->withErrors('Tài khoản không tồn tại.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        Account::where([
            'userid' => Auth::user()->id,
            'accountid' => $request->id
        ])->update([
            'reaction' => $request->reaction ?? 0,
            'target' => $request->target,
            'timeout' => $request->timeout,
            'status' => !$request->status
        ]);

        return !$request->status ? 1 : 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::where([
            'userid' => Auth::user()->id,
            'accountid' => $id,
        ])->delete();

        if ($account) {
            return back()->with('success', 'Xóa tài khoản Facebook ID ' . $id . ' thành công.');
        }

        return Redirect::back()->withErrors('Xóa tài khoản không thành công.');
    }

    public function sign_creator($username, $password)
    {
        $data = array(
            // 'api_key' => '882a8490361da98702bf97a021ddc14d', // fb for android
            'api_key' => '3e7c78e35a76a9299309885393b02d97',
            'email' => $username,
            'format' => 'JSON',
            'generate_machine_id' => '1',
            'generate_session_cookies' => '1',
            'locale' => 'vi_vn',
            'method' => 'auth.login',
            'password' => $password,
            'return_ssl_resources' => '0',
            'v' => '1.0',
        );

        $sig = "";
        foreach ($data as $key => $value) {
            $sig .= "$key=$value";
        }

        // $sig .= '62f8ce9f74b12f84c123cc23437a4a32'; // fb for android
        $sig .= 'c1e620fa708a1d5696fb991c1bde5662';
        $sig = md5($sig);
        $data['sig'] = $sig;

        $url = 'https://api.facebook.com/restserver.php?' . http_build_query($data);
        $response = JCRequest::get($url);

        return json_decode($response->body());
    }

    public function getAccountData($token)
    {
        $url = 'https://graph.facebook.com/v2.10/me?access_token=' . $token . '&debug=all&fields=id%2Cname%2Cpicture%2Cemail&format=json&method=get&pretty=0&suppress_http_code=1';
        $response = JCRequest::get($url);

        return json_decode($response->body());
    }
}
